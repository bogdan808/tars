'use strict';

// This is example of task function

var gulp = tars.packages.gulp;
var tarsConfig = tars.config;
var plumber = tars.packages.plumber;
var notifier = tars.helpers.notifier;
// Include browserSync, if you need to reload browser:
// var browserSync = tars.packages.browserSync;

// require('path to task file, which have to be done before current task');
// For example:
// require('./required-task-name');

/**
 * Task description
 */
module.exports = function () {
    return gulp.task('task:name', function (cb) {
        console.log('slowpock!!!');
        // You can return callback, if you can't return pipe
        cb();
    });
};


// module.exports = function () {
//     return gulp.task('service:clean2', function (cb) {
//         console.log('service:clean2');
//         cb();
//     });
// };
